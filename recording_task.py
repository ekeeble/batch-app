# hello
import os
import datetime
from datetime import timedelta

#if the file has not been extracted yet, then extract it
if not (os.path.isfile('wd/ffmpeg-git-20210325-amd64-static/ffmpeg')):
    os.system('tar xvf $AZ_BATCH_NODE_STARTUP_DIR/wd/ffmpeg-git-amd64-static.tar.xz')

# FFMPEG_PATH = 'ffmpeg'
FFMPEG_PATH = '$AZ_BATCH_TASK_WORKING_DIR/ffmpeg-git-20210325-amd64-static/ffmpeg'
LOG_LEVEL = '+warning'

# open file
cam_list_file = open("cam_list.txt","r")

# read file
rec_time = cam_list_file.readline().rstrip()
urls = cam_list_file.readlines()
rec_time_split = rec_time.split(':')

# get start and end time of the recording
start_time = datetime.datetime.now()
end_time = start_time + timedelta(hours=int(rec_time_split[0]), minutes=int(rec_time_split[1]))
start_hour = start_time.strftime("%H")
start_minute = start_time.strftime("%M")
end_hour = end_time.strftime("%H")
end_minute = end_time.strftime("%M")
file_time = start_hour + "-" + start_minute + "_" + end_hour + "-" + end_minute

for url in urls:
    # clean the url
    url = url.rstrip()

    # get camera name
    cam_name = url.split('/')[-2]
    cam_name = cam_name.replace("_", "")
#    mkdir_cmd = "mkdir {}".format(cam_name)
#    os.system(mkdir_cmd)

    # generate file name
    filename = cam_name + "_" + file_time + ".mp4"

    # format the command
    cmd = FFMPEG_PATH + " -loglevel " + LOG_LEVEL + " -t " + rec_time + " -i " \
            + url + " -r 15 " + filename + " &"

    # run the command
    #print(cmd)
    os.system(cmd)

#os.system('$AZ_BATCH_TASK_WORKING_DIR/ffmpeg-git-20210325-amd64-static/ffmpeg -loglevel +warning -t 00:00:30 -i https://mcleansfs2.us-east-1.skyvdn.com/rtplive/R3_115/chunklist_w1014510249.m3u8 -r 15 video.mp4')

# calculate sleep time
sleep_time = int(rec_time_split[0])*3600 + int(rec_time_split[1]*60) + int(rec_time_split[2])
sleep_cmd = "sleep " + str(sleep_time)
print(sleep_cmd)
#os.system("wait")
os.system(sleep_cmd)
cam_list_file.close()
